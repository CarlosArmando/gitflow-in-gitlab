# GitFlow In GitLab

GitFlow in GitLab

# Cambio 1
git checkout -b develop
git push -u origin develop
git checkout -b feature/login-con-facebook
touch login-con-facebook.txt
git add login-con-facebook.txt
git commit -m "Se implemento el inicio de sesion con Facebook"
git push origin feature/login-con-facebook

## Merge Request 
Menu > Merge Request > New merge request

Source branch -> Target branch
Rama origen   -> Rama Destino
feature/login-con-facebook -> develop

# Cambio 2
git checkout develop  | git pull origin develop
git checkout -b feature/exportar-reporte-drive
touch exportar-reporte-drive.txt
git add  exportar-reporte-drive.txt
git commit -m "Soporte para exportar reportes de usaurios a Google Drive"
git push -u origin feature/exportar-reporte-drive

## Merge Request 
Menu > Merge Request > New merge request

Source branch -> Target branch
Rama origen   -> Rama Destino
feature/exportar-reporte-drive -> develop

# Cambio 3 / hotfix es una rama creada para solucionar un error en produccion (main)
git checkout main | git pull origin main
git checkout -b hotfix/login-linkedin
touch login-linkedin.txt
git add login-linkedin.txt
git commit -m "Se soluciono el error al iniciar sesion con Linkedin"
git push -u origin hotfix/login-linkedin

## Merge Request 
Menu > Merge Request > New merge request

Source branch -> Target branch
Rama origen   -> Rama Destino
hotfix/login-linkedin -> main

git pull origin main
git tag -a v1.1.0 -m "version 1.1.0"
git push -u origin v1.1.0

Merge Request develop
Source branch -> Target branch
Rama origen   -> Rama Destino
main -> develop
git checkout develop
git pull origin develop

# Cambio 4
git checkout develop | git pull origin develop
git checkout -b relaese/v1.2.0
touch ajustes-relaese-v1-2-0.txt
git add ajustes-relaese-v1-2-0.txt
git commit -m "ultimos ajustes"
git push -u origin relaese/v1.2.0

## Merge Request 
Menu > Merge Request > New merge request

Source branch -> Target branch
Rama origen   -> Rama Destino
relaese/v1.2.0 -> main

git checkout main
git pull origin main
git tag -a v1.2.0 -m "version 1.2.0"
git push -u origin v1.2.0

Merge Request develop
Source branch -> Target branch
Rama origen   -> Rama Destino
main -> develop
git checkout develop
git pull origin develop